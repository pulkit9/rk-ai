import torch
import torch.nn as nn
from pytorch_pretrained_bert import BertModel

# Counter({'O': 602020, 'ID': 8405, 'BD': 2602})

def argmax(vec):
    # return the argmax as a python int
    _, idx = torch.max(vec, 1)
    return idx.item()

# Compute log sum exp in a numerically stable way for the forward algorithm
def log_sum_exp(vec):
    max_score = vec[0, argmax(vec)]
    max_score_broadcast = max_score.view(1, -1).expand(1, vec.size()[1])
    return max_score + \
        torch.log(torch.sum(torch.exp(vec - max_score_broadcast)))

def log_sum_exp_batch(log_Tensor, axis=-1): # shape (batch_size,n,m)
    max_score = torch.max(log_Tensor, axis)[0]
    # print(max_score)
    max_score_broadcast = max_score.view(log_Tensor.shape[0],-1,1)

    return max_score + torch.log(torch.exp(log_Tensor- max_score_broadcast).sum(axis))

class Bert_BiLSTM_CRF(nn.Module):
    def __init__(self, tag_to_ix, hidden_dim=768):
        super(Bert_BiLSTM_CRF, self).__init__()
        self.tag_to_ix = tag_to_ix
        self.tagset_size = len(tag_to_ix)
        # self.hidden = self.init_hidden()
        self.lstm = nn.LSTM(bidirectional=True, num_layers=2, input_size=768, hidden_size=hidden_dim//2, batch_first=True)
        self.transitions = nn.Parameter(torch.randn(
            self.tagset_size, self.tagset_size
        )) ## TRansistion Matrix - mxm size where m = tags count
        self.hidden_dim = hidden_dim
        self.start_label_id = self.tag_to_ix['[CLS]']
        self.end_label_id = self.tag_to_ix['[SEP]']
        self.fc = nn.Linear(hidden_dim, self.tagset_size)
        self.bert = BertModel.from_pretrained('scibert_scivocab_uncased')
        # self.bert.eval()  # 知用来取bert embedding
        # self.dropout = nn.Dropout()
        self.transitions.data[self.start_label_id, :] = -10000
        self.transitions.data[:, self.end_label_id] = -10000
        self.device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
        # self.transitions.to(self.device)
        


    def init_hidden(self):
        return (torch.randn(2, 1, self.hidden_dim // 2),
                torch.randn(2, 1, self.hidden_dim // 2))


    def _forward_alg(self, feats):
        '''
        this also called alpha-recursion or forward recursion, to calculate log_prob of all barX 
        '''
        T = feats.shape[1]  # sentence length
        batch_size = feats.shape[0] # batch size

        indx_O = self.tag_to_ix['O']
        indx_B = self.tag_to_ix['BD']
        indx_I = self.tag_to_ix['ID']
        
        # alpha_recursion,forward, alpha(zt)=p(zt,bar_x_1:t)
        log_alpha = torch.Tensor(batch_size, 1, self.tagset_size).fill_(-10000.).to(self.device)  #[batch_size, 1, 16]
        log_alpha_O = torch.Tensor(batch_size, 1, 1).fill_(-10000.).to(self.device)
        log_alpha_B = torch.Tensor(batch_size, 1, 1).fill_(-10000.).to(self.device)
        log_alpha_I = torch.Tensor(batch_size, 1, 1).fill_(-10000.).to(self.device)
        
        # normal_alpha_0 : alpha[0]=Ot[0]*self.PIs
        # self.start_label has all of the score. it is log,0 is p=1
        log_alpha[:, 0, self.start_label_id] = 0
        
        # feats: sentances -> word embedding -> lstm -> MLP -> feats
        # feats is the probability of emission, feat.shape=(1,tag_size) (batch, length_sentence, tagsize)
        for t in range(1, T): # iterating over every word - transitions are not word specific but emissions are - in this case feats are emmisions 
            log_alpha = (log_sum_exp_batch(self.transitions + log_alpha, axis=-1) + feats[:, t]).unsqueeze(1) # unsqueeze is to add another dimension to match the dimension of log alpha tensor [[[1,2,3,4,5,6]]]
            log_alpha_B = (log_sum_exp_batch(self.transitions + log_alpha_B) + feats[:, t, indx_B].unsqueeze(-1)).unsqueeze(1)
            log_alpha_I = (log_sum_exp_batch(self.transitions + log_alpha_I) + feats[:, t, indx_I].unsqueeze(-1)).unsqueeze(1)
            log_alpha_O = (log_sum_exp_batch(self.transitions + log_alpha_O) + feats[:, t, indx_O].unsqueeze(-1)).unsqueeze(1)
        
        log_prob_all_O = log_sum_exp_batch(log_alpha_O)
        log_prob_all_B = log_sum_exp_batch(log_alpha_B)
        log_prob_all_I = log_sum_exp_batch(log_alpha_I)
        # print("log_prob_all_O", log_prob_all_O.shape)
        log_prob_all_barX = log_sum_exp_batch(log_alpha)
        # print("log_prob_all_barX", log_prob_all_barX.shape)
        return log_prob_all_barX, log_prob_all_O, log_prob_all_B, log_prob_all_I

        
    def _score_sentence(self, feats, label_ids):
        T = feats.shape[1] # sentence length
        batch_size = feats.shape[0]
        # print("labels_id ", label_ids.shape)
        # print("feats ", feats.shape)

        batch_transitions = self.transitions.expand(batch_size,self.tagset_size,self.tagset_size)
        batch_transitions = batch_transitions.flatten(1)

        score = torch.zeros((feats.shape[0],1)).to(self.device)
        score_O = torch.zeros((feats.shape[0],1)).to(self.device)
        score_I = torch.zeros((feats.shape[0],1)).to(self.device)
        score_B = torch.zeros((feats.shape[0],1)).to(self.device)
        score_R = torch.zeros((feats.shape[0],1)).to(self.device)
        # the 0th node is start_label->start_word,the probability of them=1. so t begin with 1.
        for t in range(1, T):
            for indx in range(label_ids.shape[0]):
              if label_ids[indx, t] == 3:
                  score_O = score_O + batch_transitions.gather(-1, (label_ids[:, t]*self.tagset_size+label_ids[:, t-1]).view(-1,1))[indx] \
                      + feats[:, t].gather(-1, label_ids[:, t].view(-1,1))[indx].view(-1,1)

              elif label_ids[indx, t] == 4:
                  score_I = score_I + batch_transitions.gather(-1, (label_ids[:, t]*self.tagset_size+label_ids[:, t-1]).view(-1,1))[indx] \
                      + feats[:, t].gather(-1, label_ids[:, t].view(-1,1))[indx].view(-1,1)

              elif label_ids[indx, t] == 5:
                  score_B = score_B + batch_transitions.gather(-1, (label_ids[:, t]*self.tagset_size+label_ids[:, t-1]).view(-1,1))[indx] \
                      + feats[:, t].gather(-1, label_ids[:, t].view(-1,1))[indx].view(-1,1)

              else:
                  score_R = score_R + batch_transitions.gather(-1, (label_ids[:, t]*self.tagset_size+label_ids[:, t-1]).view(-1,1))[indx] \
                      + feats[:, t].gather(-1, label_ids[:, t].view(-1,1))[indx].view(-1,1)
                
            score = score + batch_transitions.gather(-1, (label_ids[:, t]*self.tagset_size+label_ids[:, t-1]).view(-1,1)) \
                    + feats[:, t].gather(-1, label_ids[:, t].view(-1,1)).view(-1,1)

        return score, [score_O, score_I, score_B, score_R]

    def _bert_enc(self, x, masks):
        """
        x: [batchsize, sent_len]
        enc: [batch_size, sent_len, 768]
        """
        with torch.no_grad():
            encoded_layer, _  = self.bert(x, attention_mask=masks)
            enc = encoded_layer[-1]
        return enc

    def _viterbi_decode(self, feats):
        '''
        Max-Product Algorithm or viterbi algorithm, argmax(p(z_0:t|x_0:t))
        '''
        # feats - emission matrix
        # T = self.max_seq_length
        T = feats.shape[1]
        batch_size = feats.shape[0]

        # batch_transitions=self.transitions.expand(batch_size,self.tagset_size,self.tagset_size)

        log_delta = torch.Tensor(batch_size, 1, self.tagset_size).fill_(-10000.).to(self.device)
        # [CLS] token is set 0
        log_delta[:, 0, self.start_label_id] = 0.
        
        # psi is for the vaule of the last latent that make P(this_latent) maximum.
        psi = torch.zeros((batch_size, T, self.tagset_size), dtype=torch.long)  # psi[0]=0000 useless # Batch Size, Max Sequence Length, Tag Count
        for t in range(1, T):
            # delta[t][k]=max_z1:t-1( p(x1,x2,...,xt,z1,z2,...,zt-1,zt=k|theta) )
            # delta[t] is the max prob of the path from  z_t-1 to z_t[k]
            log_delta, psi[:, t] = torch.max(self.transitions + log_delta, -1)
            # psi[t][k]=argmax_z1:t-1( p(x1,x2,...,xt,z1,z2,...,zt-1,zt=k|theta) )
            # psi[t][k] is the path choosed from z_t-1 to z_t[k],the value is the z_state(is k) index of z_t-1
            log_delta = (log_delta + feats[:, t]).unsqueeze(1)

        # trace back
        path = torch.zeros((batch_size, T), dtype=torch.long)

        # max p(z1:t,all_x|theta)
        max_logLL_allz_allx, path[:, -1] = torch.max(log_delta.squeeze(), -1)

        for t in range(T-2, -1, -1):
            # choose the state of z_t according the state choosed of z_t+1.
            path[:, t] = psi[:, t+1].gather(-1,path[:, t+1].view(-1,1)).squeeze()

        return max_logLL_allz_allx, path

    def column_sum(self, lst): 
        return [sum(i) for i in zip(*lst)]

    def neg_log_likelihood_wt_list(self, sentence, label_ids, masks):
        feats = self._get_lstm_features(sentence, masks)
        T = feats.shape[1]
        batch_size = feats.shape[0]

        weights = {'BD': 250, 'ID': 72, 'O': 1}

        batch_transitions = self.transitions.expand(batch_size, self.tagset_size, self.tagset_size)
        batch_transitions = batch_transitions.flatten(1)

        score = torch.zeros((feats.shape[0], 1)).to(self.device)
        score_O = torch.zeros((feats.shape[0], 1)).to(self.device)
        score_I = torch.zeros((feats.shape[0], 1)).to(self.device)
        score_B = torch.zeros((feats.shape[0], 1)).to(self.device)
        score_R = torch.zeros((feats.shape[0], 1)).to(self.device)

        indx_O = self.tag_to_ix['O']
        indx_B = self.tag_to_ix['BD']
        indx_I = self.tag_to_ix['ID']

        log_alpha = torch.Tensor(batch_size, 1, self.tagset_size).fill_(-10000.).to(self.device)  # [batch_size, 1, 6]

        losses = []

        for t in range(1, T):
            log_alpha = (log_sum_exp_batch(self.transitions + log_alpha, axis=-1) + feats[:, t]).unsqueeze(1)  
            # unsqueeze is to add another dimension to match the dimension of log alpha tensor [[[1,2,3,4,5,6]]]
            score = score + batch_transitions.gather(-1, (label_ids[:, t] * self.tagset_size + label_ids[:, t - 1]).view(-1, 1)) \
                    + feats[:, t].gather(-1, label_ids[:, t].view(-1, 1)).view(-1, 1)
            
            for indx in range(label_ids.shape[0]):
                if label_ids[indx, t] == 3:
                    score_O = score_O + batch_transitions.gather(-1, (
                                label_ids[:, t] * self.tagset_size + label_ids[:, t - 1]).view(-1, 1))[indx] \
                              + feats[:, t].gather(-1, label_ids[:, t].view(-1, 1))[indx].view(-1, 1)

                    temp = torch.logsumexp(log_alpha, 1)
                    log_alpha_O2 = temp[:, 3].reshape(-1, 1)
                    losses.append(weights['O']*torch.mean((log_alpha_O2 - score_O)/batch_size))

                elif label_ids[indx, t] == 4:
                    score_I = score_I + batch_transitions.gather(-1, (
                                label_ids[:, t] * self.tagset_size + label_ids[:, t - 1]).view(-1, 1))[indx] \
                              + feats[:, t].gather(-1, label_ids[:, t].view(-1, 1))[indx].view(-1, 1)

                    temp = torch.logsumexp(log_alpha, 1)
                    log_alpha_I2 = temp[:, 4].reshape(-1, 1)
                    losses.append(weights['ID']*torch.mean((log_alpha_I2 - score_I)/batch_size))

                elif label_ids[indx, t] == 5:
                    score_B = score_B + batch_transitions.gather(-1, (
                                label_ids[:, t] * self.tagset_size + label_ids[:, t - 1]).view(-1, 1))[indx] \
                              + feats[:, t].gather(-1, label_ids[:, t].view(-1, 1))[indx].view(-1, 1)

                    temp = torch.logsumexp(log_alpha, 1)
                    log_alpha_B2 = temp[:, 5].reshape(-1, 1)
                    losses.append(weights['BD']*torch.mean((log_alpha_B2 - score_B)/batch_size))

                else:
                    score_R = score_R + batch_transitions.gather(-1, (
                                label_ids[:, t] * self.tagset_size + label_ids[:, t - 1]).view(-1, 1))[indx] \
                              + feats[:, t].gather(-1, label_ids[:, t].view(-1, 1))[indx].view(-1, 1)

                    temp = torch.logsumexp(log_alpha, 1)
                    log_alpha_R2 = temp[:, [1,2]]
                    log_alpha_temp = log_alpha_R2.sum(dim=1).reshape(-1, 1)
                    losses.append(torch.mean(log_alpha_temp - score_R))
        return losses, feats

    def neg_log_likelihood_wt(self, sentence, tags, masks):
        feats = self._get_lstm_features(sentence, masks)  #[batch_size, max_len, 6]
        forward_score, fwd_score_O, fwd_score_B, fwd_score_I = self._forward_alg(feats)
        gold_score, tag_score = self._score_sentence(feats, tags)
        if len(tag_score[0]) > 0:
          score_O = tag_score[0]
        if len(tag_score[1]) > 0:
          score_ID = tag_score[1]
        if len(tag_score[2]) > 0:
          score_BD = tag_score[2]

        weights = {'BD': 250, 'ID': 72, 'O': 1}
        weighted_score = score_O*weights['O'] + score_BD*weights['BD'] + score_ID*weights['ID']
        weighted_fwd_score = weights['O']*fwd_score_O + weights['BD']*fwd_score_B + weights['ID']*fwd_score_I
        weighted_diff_score = weights['O'] * (fwd_score_O - score_O) + weights['BD'] * (fwd_score_B - score_BD) + weights['ID'] * (fwd_score_I - score_ID)
        unweighted_diff_score = (fwd_score_O - score_O) + (fwd_score_B - score_BD) + (fwd_score_I - score_ID)
        return torch.mean(weighted_diff_score), feats

    def neg_log_likelihood(self, sentence, tags, masks):
        feats = self._get_lstm_features(sentence, masks)  #[batch_size, max_len, 6]
        forward_score, fwd_score_O, fwd_score_B, fwd_score_I = self._forward_alg(feats)
        gold_score, tag_score = self._score_sentence(feats, tags)

        loss = forward_score - gold_score
        return loss.mean(), feats


    def _get_lstm_features(self, sentence, masks):
        """sentence is the ids"""
        # self.hidden = self.init_hidden()
        embeds = self._bert_enc(sentence, masks)  # [8, 75, 768]
        # print("embeds ", embeds.shape)
        enc, _ = self.lstm(embeds)
        # print("enc ", enc.shape)
        lstm_feats = self.fc(enc)
        return lstm_feats  # [8, 75, 6]

    def forward(self, sentence, masks):
        # Get the emission scores from the BiLSTM
        lstm_feats = self._get_lstm_features(sentence, masks)  # [8, 180,768]

        # Find the best path, given the features.
        score, tag_seq = self._viterbi_decode(lstm_feats)
        return score, tag_seq
    

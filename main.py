import torch
import torch.nn as nn
import torch.optim as optim
import os
import numpy as np
import argparse
from torch.utils import data
from crf import Bert_BiLSTM_CRF
from utils import NerDataset, pad, VOCAB, tokenizer, tag2idx, idx2tag
from sklearn.metrics import f1_score, classification_report, recall_score, precision_score
from itertools import compress

import matplotlib.pyplot as plt
from model_lr import Net
import pandas as pd

parser = argparse.ArgumentParser()
parser.add_argument("--batch_size", type=int, default=64)
parser.add_argument("--lr", type=float, default=0.0001)
parser.add_argument("--n_epochs", type=int, default=30)
parser.add_argument("--finetuning", type=bool, default=False)
parser.add_argument("--top_rnns", type=bool, default=False)
parser.add_argument("--logdir", type=str, default="checkpoints/01")
parser.add_argument("--trainset", type=str, default="processed/train.txt")
parser.add_argument("--validset", type=str, default="processed/val.txt")
parser.add_argument("--loss_list", type=bool, default=False)
parser.add_argument("--loss_weighted", type=bool, default=False)
parser.add_argument("--loss_original", type=bool, default=False)
parser.add_argument("--loss_crossentropy", type=bool, default=False)
parser.add_argument("--crf", type=bool, default=False)
parser.add_argument("--logr", type=bool, default=False)
hp = parser.parse_args()


def train(model, iterator, optimizer, criterion, device):
    data = []
    model.train()
    print(len(iterator))
    loss_list = []
    running_loss = 0.0
    for i, batch in enumerate(iterator):
        words, x, is_heads, tags, y, seqlens = batch
        x = x.to(device)
        y = y.to(device)
        is_heads = is_heads.to(device)
        _y = y # for monitoring
        y_focal = y.view(-1)

        optimizer.zero_grad()
        if hp.loss_original:
            loss, lstm_feats = model.neg_log_likelihood(x, y, is_heads)
            loss.backward()

        if hp.loss_list:
            loss, lstm_feats = model.neg_log_likelihood_wt_list(x, y, is_heads)
            torch.autograd.backward(loss)

        if hp.loss_weighted:
            loss, lstm_feats = model.neg_log_likelihood_wt(x, y, is_heads)
            loss.backward()

        if hp.loss_crossentropy:
            neg_loss, lstm_feats = model.neg_log_likelihood(x, y, is_heads)
            logits_focal = lstm_feats.view(-1, lstm_feats.shape[-1])
            crit_loss = criterion(logits_focal, y_focal)
            loss = neg_loss + crit_loss
            loss.backward()
            running_loss += loss.item() * lstm_feats.shape[0]

        if hp.logr:
            output = model(x, is_heads)
            logits_focal = output.view(-1, output.shape[-1])
            loss = criterion(logits_focal, y_focal)
            loss.backward()
            running_loss += loss.item() * output.shape[0]
            for word, tag, pred in zip(words, y, output):
              for w, t, p in zip(word.split(), tag, pred):
                data.append([w, t.cpu().detach().numpy(), p.cpu().detach().numpy().tolist()])

        optimizer.step()

        if i==0:
            print("=====sanity check======")
            print("x:", x.cpu().numpy()[0][:seqlens[0]])
            print("is_heads:", is_heads[0])
            print("y:", _y.cpu().numpy()[0][:seqlens[0]])
            print("tags:", tags[0])
            print("seqlen:", seqlens[0])
            print("=======================")

        if i%10==0:
            print(f"step: {i}, loss: {loss.item()}")
    # print(data)
    df = pd.DataFrame(data, columns=["Word", "Tag", "Pred Tensor"])
    df.to_csv('data.csv', index=False)

    return running_loss

def eval(model, iterator, f, device, length_eval):
    model.eval()
    f1_lib = 0.0
    Words, Is_heads, Tags, Y, Y_hat = [], [], [], [], []
    with torch.no_grad():
        for i, batch in enumerate(iterator):
            tag = []
            word_new = []
            words, x, is_heads, tags, y, seqlens = batch
            x = x.to(device)
            is_heads = is_heads.to(device)
            y = y.to(device)
            if hp.crf:
              _, y_hat = model(x, is_heads)  # y_hat: (N, T)
            if hp.logr:
              y_hat = model(x, is_heads)
            
            preds_mask = (
                (y != tag2idx['[CLS]'])
                & (y != tag2idx['<PAD>'])
                & (y != tag2idx['[SEP]'])
                )
            # ps = torch.exp(y_hat)
            # print(ps)
            # print("y_hat ", y_hat.shape)
            # print("ps", ps.shape)
            # ps_max, ps_indx = torch.max(ps, dim=-1)
            # print("ps_indx", ps_indx.shape)
            preds_mask = preds_mask.to(device)
            y_hat = y_hat.to(device)
            
            label_ids = torch.masked_select(y, (preds_mask == 1))
            val_preds = torch.masked_select(y_hat, (preds_mask == 1))
            for t in tags:
              temp = t.split()
              temp2 = []
              for tmp in temp:
                if tmp in ['[SEP]', '[CLS]', '<PAD>']:
                  pass 
                else:
                  temp2.append(tmp)
              tag.extend(temp2)
            for t in words:
              temp = t.split()
              temp2 = []
              for tmp in temp:
                if tmp in ['[SEP]', '[CLS]', '[PAD]']:
                  pass 
                else:
                  temp2.append(tmp)
              word_new.extend(temp2)
            val_labels = label_ids.to(device)
            Words.extend(word_new)
            Is_heads.extend(is_heads)
            Tags.extend(tag)
            Y.extend(val_labels.cpu().numpy().tolist())
            Y_hat.extend(val_preds.cpu().numpy().tolist())

    report = classification_report(Y, Y_hat)
    f1score = f1_score(Y, Y_hat, average='weighted')
    print(report)
    print("F1 Score ", f1score)
    print("Precision Score ", precision_score(Y, Y_hat, average='weighted'))
    print("Recall Score ", recall_score(Y, Y_hat, average='weighted'))
    return f1score

if __name__=="__main__":
    print(os.getcwd())
    device = 'cuda' if torch.cuda.is_available() else 'cpu'

    if hp.crf:
        model = Bert_BiLSTM_CRF(tag2idx).cuda() if device == 'cuda' else Bert_BiLSTM_CRF(tag2idx)
    if hp.logr:
        model = Net(tag2idx).cuda() if device == 'cuda' else Net(tag2idx)
    print('Initial model Done')

    train_dataset = NerDataset(hp.trainset)
    eval_dataset = NerDataset(hp.validset)
    print('Load Data Done')

    train_iter = data.DataLoader(dataset=train_dataset,
                                 batch_size=hp.batch_size,
                                 shuffle=True,
                                 num_workers=4,
                                 collate_fn=pad)
    eval_iter = data.DataLoader(dataset=eval_dataset,
                                 batch_size=hp.batch_size,
                                 shuffle=False,
                                 num_workers=4,
                                 collate_fn=pad)

    weights = {'BD': 20, 'ID': 4, 'O': 1}

    optimizer = optim.Adam(model.parameters(), lr = hp.lr)
    weights = torch.FloatTensor([0,0,0,20,4,1]).to(device)
    criterion = nn.CrossEntropyLoss(weight=weights) 

    print('Start Train...')
    max_f1 = 0.0
    plot_loss = []
    for epoch in range(1, hp.n_epochs+1):

        running_loss = train(model, train_iter, optimizer, criterion, device)

        epoch_loss = running_loss / len(train_iter)
        plot_loss.append(epoch_loss)

        print(f"=========eval at epoch={epoch}=========")
        if not os.path.exists(hp.logdir): os.makedirs(hp.logdir)
        fname = os.path.join(hp.logdir, "best")
        f1 = eval(model, eval_iter, fname, device, int(len(eval_dataset)))
        if max_f1 < f1:
          max_f1 = f1
          torch.save(model.state_dict(), f"{fname}.pt")
          print(f"weights were saved to {fname}.pt")

    plt.plot(plot_loss)
    plt.title('Training Loss')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.show()
    plt.savefig('plot.png')

from pytorch_pretrained_bert import BertModel
import torch.nn as nn
import torch
import torch.nn.functional as F

class Net(nn.Module):
    def __init__(self, tag_to_ix, hidden_dim=768):
        super(Net, self).__init__()
        self.tag_to_ix = tag_to_ix
        self.tagset_size = len(tag_to_ix)
        self.bilstm = nn.LSTM(bidirectional=True, num_layers=2, input_size=768, hidden_size=hidden_dim//2, batch_first=True)
        self.lstm = nn.LSTM(input_size=768, num_layers=2, hidden_size=hidden_dim//2, batch_first=True)
        self.hidden_dim = hidden_dim
        self.start_label_id = self.tag_to_ix['[CLS]']
        self.end_label_id = self.tag_to_ix['[SEP]']
        self.fc = nn.Linear(384, self.tagset_size)
        self.bert = BertModel.from_pretrained('scibert_scivocab_uncased')
        self.device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    def _bert_enc(self, x, masks):
        """
        x: [batchsize, sent_len]
        enc: [batch_size, sent_len, 768]
        """
        with torch.no_grad():
            encoded_layer, _  = self.bert(x, attention_mask=masks)
            enc = encoded_layer[-1]
            # print("enc ", enc.shape)
        return enc

    def forward(self, sentence, masks):
        embeds = self._bert_enc(sentence, masks)  # [8, 75, 768]
        enc, _ = self.bilstm(embeds)
        lsenc, _ = self.lstm(enc)
        lstm_feats = self.fc(lsenc)
        x = F.log_softmax(lstm_feats)
        # print("lstm feats ", lstm_feats.shape)
        # print("x - log_softmax", x.shape)
        return x  # [8, 75, 6]
import torch
from torch.utils import data
import argparse

import os
import sys
import numpy as np
import pandas as pd

from utils import tag2idx, idx2tag, NerDataset, pad
from crf import Bert_BiLSTM_CRF
from model_lr import Net
from sklearn.metrics import f1_score, classification_report, recall_score, precision_score
print(idx2tag)

parser = argparse.ArgumentParser()
parser.add_argument("--crf", type=bool, default=False)
parser.add_argument("--logr", type=bool, default=False)
parser.add_argument("--input_file", type=str, default="processed/test.txt")
parser.add_argument("--weights", type=str, default="checkpoints/01/best.pt")
hp = parser.parse_args()

CRF_MODEL_PATH = hp.weights

def test(iterator, model, device, length_test):
    pred_tags = []
    f1_lib = 0.0
    Words, Is_heads, Tags, Y, Y_hat = [], [], [], [], []
    print(len(iterator))
    with torch.no_grad():
        for i, batch in enumerate(iterator):
            tag = []
            word_new = []
            words, x, is_heads, tags, y, seqlens = batch
            x = x.to(device)
            y = y.to(device)
            is_heads = is_heads.to(device) 

            if hp.crf:
              _, y_hat = model(x, is_heads)  # y_hat: (N, T)
            if hp.logr:
              y_hat = model(x, is_heads)
            preds_mask = (
                (y != tag2idx['[CLS]'])
                & (y != tag2idx['<PAD>'])
                & (y != tag2idx['[SEP]'])
                )
            # y_hat_max, y_hat_indx = torch.max(y_hat, dim=-1)
            preds_mask = preds_mask.to(device)
            y_hat = y_hat.to(device)
            # y_hat_max = y_hat_max.to(device).to(dtype=torch.float64)
            label_ids = torch.masked_select(y, (preds_mask == 1))
            val_preds = torch.masked_select(y_hat, (preds_mask == 1))
            if i==0:
              print("y_hat ", y_hat.cpu().numpy().tolist())
              print("y_true ", y.cpu().numpy().tolist())
              print("is_head ", is_heads)
            for t in tags:
              temp = t.split()
              temp2 = []
              for tmp in temp:
                if tmp in ['[SEP]', '[CLS]', '<PAD>']:
                  pass 
                else:
                  temp2.append(tmp)
              tag.extend(temp2)
            for t in words:
              temp = t.split()
              temp2 = []
              for tmp in temp:
                if tmp in ['[SEP]', '[CLS]', '[PAD]']:
                  pass 
                else:
                  temp2.append(tmp)
              word_new.extend(temp2)
            val_labels = label_ids.to(device)
            Words.extend(word_new)
            Is_heads.extend(is_heads.cpu().numpy().tolist())
            Tags.extend(tag)
            Y.extend(val_labels.cpu().numpy().tolist())
            Y_hat.extend(val_preds.cpu().numpy().tolist())

    report = classification_report(Y, Y_hat, digits=2, zero_division=1)
    f1score = f1_score(Y, Y_hat, average='weighted')
    print(report)
    print("F1 Score ", f1score)
    print("Precision Score ", precision_score(Y, Y_hat, average='weighted'))
    print("Recall Score ", recall_score(Y, Y_hat, average='weighted'))
    data = []
    print(len(Words))
    print(len(Y))
    print(len(Y_hat))
    for w, y, y_h in zip(Words, Y, Y_hat):
      data.append([w, y, y_h])
    
    df = pd.DataFrame(data, columns=['Words', 'Y_True', 'Y_Pred'])
    df.to_csv('output.csv', index=False)
    return f1score 


def main():
    device = 'cuda' if torch.cuda.is_available() else 'cpu'

    test_path = hp.input_file
    if hp.crf:
      model = Bert_BiLSTM_CRF(tag2idx)
    if hp.logr:
      model = Net(tag2idx)
    model.load_state_dict(torch.load(CRF_MODEL_PATH))
    model.to(device)
    model.eval()
    test_dataset = NerDataset(test_path)
    print('Test Data Loaded..')

    test_iter = data.DataLoader(dataset=test_dataset,
                                batch_size=64,
                                shuffle=False,
                                num_workers=4,
                                collate_fn=pad)

    f1score = test(test_iter, model, device, int(len(test_dataset)))


if __name__ == '__main__':
    main()
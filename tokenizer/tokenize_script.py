'''
Author: Nandini Bansal
Date: 29th May 2020
Before running the script, kindly ensure that scibert model weights are downloaded and the folder path is passed to --bert_model cmd argument.
'''
from pytorch_pretrained_bert import BertTokenizer
import argparse
import os
import errno
import sys
import random
import collections
import numpy as np
import math
import re

parser = argparse.ArgumentParser()
parser.add_argument('--input_file', default='sentences.txt', help="STRING - path to input text file")
parser.add_argument('--verbose', default=True, help="BOOLEAN - print information during execution")
parser.add_argument('--output_file', default="sentences_op.txt", help="STRING - path to output text file")
parser.add_argument('--bert_model', default="scibert_scivocab_uncased", help="STRING - model name or path to the model")
args = parser.parse_args()

###### ----- GLOABLS ---------------
bert_model = args.bert_model
if args.verbose:
    print(f'Loading {bert_model}....')
tokenizer = BertTokenizer.from_pretrained(bert_model)

def create_class_weight(labels_dict):
    total = sum(labels_dict.values())
    keys = labels_dict.keys()
    class_weight = dict()
    print(total)

    for key in keys:
        score = total/float(labels_dict[key])
        class_weight[key] = round(score, 0) if score > 1.0 else 1.0

    return class_weight


def main():
    if not os.path.exists(args.input_file):
        print(FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.input_file))
        sys.exit(-1)

    if args.verbose:
        print(f"Opening {args.input_file}..")
    with open(args.input_file, "r", encoding="utf-8") as fobj:
        token_tags = fobj.readlines()


    if args.verbose:
        print(f"Total Lines in input file : {len(token_tags)}")

    token_list = []
    tag_list = []
    for line in token_tags:
        line = line.strip()
        print(line)
        if line != "":
            token = line.split()[0]
            tag = re.findall(r'"([^"]*)"', line)[0]
            if "BD" in tag:
                tag = "BD"
            elif "ID" in tag:
                tag = "ID"
            else:
                tag = "O"
            token_list.append(token)
            tag_list.append(tag)
              
    bert_token = []
    bert_tag = []
    for t, tag in zip(token_list, tag_list):
      tok = tokenizer.tokenize(t)
      if len(tok) > 1:
        if tag == "BD":
          tags = ["BD"]
          new_tags = ["ID"]*(len(tok)-1)
          tags.extend(new_tags)
        else:
          tags = [tag]*len(tok)
      else:
        tags = [tag]
      bert_tag.extend(tags)
      bert_token.extend(tok)
    dataset = []
    with open(args.output_file, 'w', encoding="utf-8") as filehandle:
        for token, tag in zip(bert_token, bert_tag):
          dataset.append([token, tag])
          # print(token, "\t", tag)
          filehandle.write(token+"  "+tag+"\n")


    # random.seed(2019)
    # random.shuffle(dataset)

    test_length = int(0.2 * len(dataset))
    train_val_length = int(len(dataset) - test_length)
    if args.verbose:
      print(f"Train Dataset {train_val_length} out of {len(dataset)}")
    dataset_train_val = dataset[:train_val_length]
    dataset_test = dataset[train_val_length:]

    val_length = int(0.2 * len(dataset_train_val))
    train_length = (len(dataset_train_val) - val_length)

    dataset_train = dataset[:train_length]
    dataset_val = dataset[train_length:]

    print(len(dataset_train))
    print(len(dataset_val))
    print(len(dataset_test))

    with open('train.txt', 'w', encoding="utf-8") as filehandle:
        for token, tag in dataset_train:
          # print(token, "\t", tag)
          filehandle.write(token+"\t"+tag+"\n")
    with open('val.txt', 'w', encoding="utf-8") as filehandle:
        for token, tag in dataset_val:
          # print(token, "\t", tag)
          filehandle.write(token+"\t"+tag+"\n")
    with open('test.txt', 'w', encoding="utf-8") as filehandle:
        for token, tag in dataset_test:
          # print(token, "\t", tag)
          filehandle.write(token+"\t"+tag+"\n")

    counter=collections.Counter(bert_tag)
    print(dict(counter))

    print(create_class_weight(dict(counter)))


if __name__ == '__main__':
    main()
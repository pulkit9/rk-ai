import os
import numpy as np
import logging
import torch
from torch.utils.data import Dataset
from typing import Tuple, List
from pytorch_pretrained_bert import BertTokenizer

logger = logging.getLogger(__name__)

bert_model = 'scibert_scivocab_uncased'
tokenizer = BertTokenizer.from_pretrained(bert_model)
VOCAB = ('<PAD>', '[CLS]', '[SEP]', 'O', 'ID', 'BD')
tag2idx = {tag: idx for idx, tag in enumerate(VOCAB)}
idx2tag = {idx: tag for idx, tag in enumerate(VOCAB)}
MAX_LEN = 256 - 2


class NerDataset(Dataset):
    def __init__(self, f_path):
        with open(f_path, 'r', encoding='utf-8') as fr:
            entries = fr.read().strip().split('\n\n')
        sents, tags_li = [], [] # list of lists
        for entry in entries:
            words = [line.split()[0] for line in entry.splitlines()]
            tags = ([line.split()[-1] for line in entry.splitlines()])
            if len(words) > MAX_LEN:
                word, tag = [], []
                for char, t in zip(words, tags):
                    word.append(char)
                    tag.append(t)

                    if len(word) == (MAX_LEN):
                        sents.append(["[CLS]"] + word + ["[SEP]"])
                        tags_li.append(['[CLS]'] + tag + ['[SEP]'])
                        word = []
                        tag = []
                else:
                    if len(word) > 0:
                        sents.append(["[CLS]"] + word + ["[SEP]"])
                        tags_li.append(['[CLS]'] + tag + ['[SEP]'])
        self.sents, self.tags_li = sents, tags_li
                

    def __getitem__(self, idx):
        words, tags = self.sents[idx], self.tags_li[idx]
        x, y = [], []
        is_heads = []
        for w, t in zip(words, tags):
            xx = tokenizer.convert_tokens_to_ids([w])
            is_head = [1]
            t = [t]
            yy = [tag2idx[each] for each in t]

            x.extend(xx)
            is_heads.extend(is_head)
            y.extend(yy)
        assert len(x)==len(y)==len(is_heads), f"len(x)={len(x)}, len(y)={len(y)}, len(is_heads)={len(is_heads)}"

        # seqlen
        seqlen = len(y)

        # to string
        words = " ".join(words)
        tags = " ".join(tags)
        return words, x, is_heads, tags, y, seqlen


    def __len__(self):
        return len(self.sents)


def add_padding(sample, indx, maxlen, tkn):
    word = sample[indx]
    words = word.split()
    wlen = len(words)
    padded_words = words + [tkn] * (maxlen - wlen)
    return ' '.join(padded_words)


def pad(batch):
    '''Pads to the longest sample'''
    fetch_data = lambda x: [sample[x] for sample in batch]
    # words = f(0)
    # is_heads = f(2)
    # tags = f(3)
    seqlens = fetch_data(-1)
    maxlen = np.array(seqlens).max()

    add_ids = lambda x, seqlen: [sample[x] + [tag2idx['<PAD>']] * (seqlen - len(sample[x])) for sample in batch] # 0: <pad>
    x = add_ids(1, maxlen)
    y = add_ids(-2, maxlen)
    is_heads = add_ids(2, maxlen)

    add_tags = lambda x, seqlen: [add_padding(sample, x, seqlen, '<PAD>') for sample in batch]
    add_tokens = lambda x, seqlen: [add_padding(sample, x, seqlen, '[PAD]') for sample in batch]
    tags = add_tags(3, maxlen)
    words = add_tokens(0, maxlen)
    # print(tags)

    padded_len = lambda x: [len(sample[x]) for sample in batch]
    seqlens = padded_len(-2)

    f = torch.LongTensor

    return words, f(x), f(is_heads), tags, f(y), seqlens
    
